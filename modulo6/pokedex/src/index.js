/* let dadosPokemon = document.getElementById('dadosPokemon');

let pokeApi = new PokeApi();
let consultarJson = pokeApi.buscarEspecifico(112);
consultarJson.then( pokemon => {
    let nome = dadosPokemon.querySelector(".nome");
    nome.innerHTML = pokemon.name;

    let imgPokemon = dadosPokemon.querySelector('.thumb');
    imgPokemon.src = pokemon.sprites.front_default;

    console.log( `Pokemon: ${ pokemon.name }` );
}); */

let pokeApi = new PokeApi();
pokeApi
    .buscarEspecifico(112)
    .then( pokemon => {
            let poke = new Pokemon( pokemon );
            renderizar( poke );
        }
);

/* const pokeApi = new PokeApi();
async function rodar() {
    const pokemonServidor = await pokeApi.buscarEspecifico(112);
    let poke = new Pokemon( pokemonServidor );
    renderizar( poke );
}
rodar(); */


renderizar = ( pokemon ) => {
    let dadosPokemon = document.getElementById('dadosPokemon');
    let nome = dadosPokemon.querySelector(".nome");
    nome.innerHTML = pokemon.nome;

    let imgPokemon = dadosPokemon.querySelector('.thumb');
    imgPokemon.src = pokemon.imagem;

    let altura = dadosPokemon.querySelector('.altura');
    altura.innerHTML = pokemon.altura;
}

