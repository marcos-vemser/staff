class Pokemon {
    constructor( objDaAPI ) {
        this._nome = objDaAPI.name;
        this._imagem = objDaAPI.sprites.front_default;
        this._altura = objDaAPI.height;
    }

    get nome(){
        return this._nome;
    }

    get imagem() {
        return this._imagem;
    }

    get altura() {
        return `${this._altura * 10} cm` ;
    }
}