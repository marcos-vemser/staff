import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Home from './home';
import ListaAvaliacoes from './avaliacoes';
import DetalhesEpisodios from './detalhesEpisodios';
import TodosEpisodios from './todosEpisodios';

export default class App extends Component {
  render(){
    return (
      <div className="App">
        <Router>
          <Route path="/" exact component={ Home } />
          <Route path="/avaliacoes" exact component={ ListaAvaliacoes } />
          <Route path="/episodios" exact component={ TodosEpisodios } />
          <Route path="/episodio/:id" exact component={ DetalhesEpisodios } />
        </Router>
      </div>
    )
  };
}

/* const ListaAvaliacoes = () => <h2>Lista Avaliações</h2> */