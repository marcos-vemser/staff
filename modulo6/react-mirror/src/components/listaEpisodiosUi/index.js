import React from 'react';
import { Link } from 'react-router-dom';

const ListaEpisodios = ( { listaEpisodios } ) =>
  <React.Fragment>
    {
      listaEpisodios && listaEpisodios.map( e => 
        <li key={ e.id }>
          <Link to={{ pathname: `/episodio/${ e.id }`, state: { episodio: e } }}>
            { `${ e.nome } - ${ e.nota || 'Sem nota' } - ${ e.dataEstreia.toLocaleDateString( 'pt-BR' ) || 'N/D' }` }
          </Link>
        </li>
      )
    }
  </React.Fragment>

export default ListaEpisodios;