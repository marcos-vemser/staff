import React, { Component } from 'react';
import './App.scss';
import ListaSeries from '../models/listaSeries';

export default class App extends Component {
  constructor( props ) {
    super( props );
    const listaSeries = new ListaSeries();
    
    console.log(listaSeries.invalidas());
    console.log(listaSeries.filtrarPorAno( 2017 ));
    console.log(listaSeries.procurarPorNome( 'Winona Ryder' ));
    console.log(listaSeries.mediaDeEpisodios());
    console.log(listaSeries.totalSalarios(0));
    console.log(listaSeries.queroGenero('Caos'));
    console.log(listaSeries.queroTitulo( 'The' ));
    console.log(listaSeries.creditos( 2 ));
    console.log(listaSeries.hashtag());
  }

  render(){
    return ( 
      <div className="App">
        
      </div>
    )
  };
}