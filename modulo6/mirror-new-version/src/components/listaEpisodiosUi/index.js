import React from 'react';
import { Link } from 'react-router-dom';

import { Cartao, Corpo } from '../card';

const ListaEpisodios = ( { listaEpisodios } ) =>
  <React.Fragment>
    { listaEpisodios && listaEpisodios.map( e => 
        <Link to={{ pathname: `/episodio/${ e.id }`, state: { episodio: e } }}>
          <Cartao stilo={{ width: 250, height: 430 }} imagem={ e.url } textoAlt={ e.nome }>
            <Corpo titulo={ e.nome } descricao={ e.sinopse } />
            <h4>Duração: { e.duracaoEmMin }</h4>
            <h4>Nota: { e.duracaoEmMin }</h4>
          </Cartao>
        </Link>
      )
    }
  </React.Fragment>

export default ListaEpisodios;