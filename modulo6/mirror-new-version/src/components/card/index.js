import React from 'react';
import { Card } from 'antd';

const { Meta } = Card;

const Cartao = ({ stilo, imagem, textoAlt, children }) => 
  <Card
    hoverable
    style={ stilo }
    cover={<img alt={ textoAlt } src={ imagem } />}
  >
    { children }
  </Card>;

const Corpo = ({ titulo, descricao }) => <Meta title={ titulo } description={ descricao } />;

export { Cartao, Corpo };