import React, { Component } from 'react';

import './Home.scss';

import { EpisodioUi,
         BotaoUi,
         MensagemFlash,
         MeuInputNumero,
         Lista
} from '../../components';

import { ListaEpisodios, EpisodiosApi } from '../../models';

import Mensagens from '../../constants/mensagens';

export default class Home extends Component {
  constructor( props ) {
    super( props );
    this.episodiosApi = new EpisodiosApi();
    this.marcarComoAssistido = this.marcarComoAssistido.bind( this );
    this.state = {
      deveExibirMensagem: false,
      deveExibirErro: false,
      mensagem: ''
    };
  }

  componentDidMount() {
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodasNotas()
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        const episodiosDoServidor = respostas[ 0 ];
        const notasServidor = respostas[ 1 ];
        this.listaEpisodios = new ListaEpisodios( episodiosDoServidor, notasServidor );
        this.setState( state => { return { ...state, episodio: this.listaEpisodios.episodiosAleatorios } } );
      })
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios;
    this.setState((state) => {
      return { ...state, episodio }
    })
  }

  marcarComoAssistido() {
    const { episodio } = this.state;
    episodio.marcarComoAssistido();
    this.setState( state => {
      return { ...state, episodio }
    })
  }

  registrarNota( { nota, erro } ) {
    this.setState( state => {
      return {
        ...state,
        deveExibirErro: erro
      }
    });
    if( erro ) {
      return;
    }

    const { episodio } = this.state;
    let corMensagem, mensagem;
    
    if (episodio.validarNota( nota )) {
      episodio.avaliar( nota ).then( () => {
        corMensagem = 'verde';
        mensagem = Mensagens.SUCESSO.REGISTRO_NOTA;
        this.exibirMensagem( { corMensagem, mensagem } );
      });
    }else{
      corMensagem = 'vermelho';
      mensagem = Mensagens.ERRO.NOTA_INVALIDA;
      this.exibirMensagem( { corMensagem, mensagem } );
    }
  }

  exibirMensagem = ({ corMensagem, mensagem }) => {
    this.setState( state => {
      return {
        ...state,
        deveExibirMensagem: true,
        mensagem,
        corMensagem
      }
    });
  }

  atualizarMensagem = devoExibir => {
    this.setState( state => {
      return { 
        ...state,
        deveExibirMensagem: devoExibir
      }
    });
  }

  linha( item, i ) {
    return <BotaoUi key={ i } classe={ item.cor } nome={ item.nome } metodo={ item.metodo } />
  }

  render(){
    const { episodio, deveExibirMensagem, mensagem, corMensagem, deveExibirErro } = this.state;
    const { listaEpisodios } = this;

    return ( !episodio ? (
                <h3>Aguarde...</h3>
              ) : (
                <React.Fragment>
                  <MensagemFlash
                        atualizarMensagem={ this.atualizarMensagem }
                        mensagem={ mensagem }
                        cor={ corMensagem }
                        exibir={ deveExibirMensagem }
                        segundos="5" />
                  <header className="App-header">
                    <BotaoUi link={{ pathname: "/avaliacoes", state: { listaEpisodios } }} nome="Lista de avaliações" />
                    <BotaoUi link={{ pathname: "/episodios", state: { listaEpisodios } }} nome="Todos episódios" />
                    <EpisodioUi episodio={ episodio } />
                    <Lista
                        classeName="botoes"
                        dados={ [
                            { cor: "verde", nome: "Próximo", metodo: this.sortear.bind( this ) },
                            { cor: "azul", nome: "Já Assisti!", metodo: this.marcarComoAssistido }
                        ] }
                        funcao={ ( item, i ) => this.linha( item, i ) } />
                    <MeuInputNumero
                            className="descricao-nota"
                            placeholder="Nota de 1 a 5"
                            mensagem="Qual a sua nota para esse episódio?"
                            obrigatorio={ true }
                            visivel={ episodio.assistido || false }
                            deveExibirErro={ deveExibirErro }
                            atualizarValor={ this.registrarNota.bind( this ) } />
                  </header>
                </React.Fragment>
              )
    )
  };
}