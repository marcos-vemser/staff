package br.com.dbccompany.vemsercoworking.Service;

import br.com.dbccompany.vemsercoworking.Entity.EntityAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public abstract class ServiceAbstract<
        R extends CrudRepository<E, T>,
        E extends EntityAbstract, T> {
    @Autowired
    protected R repository;

    @Transactional(rollbackFor = Exception.class)
    public E salvar(E entidade) {
        return repository.save(entidade);
    }

    @Transactional(rollbackFor = Exception.class)
    public E editar(E entidade, T id) {
        entidade.setId(id);
        return repository.save(entidade);
    }

    public List<E> todos() {
        return (List<E>) repository.findAll();
    }

    public E porId(T id) {
        return repository.findById(id).get();
    }

}
