package br.com.dbccompany.vemsercoworking.Service;

import br.com.dbccompany.vemsercoworking.Entity.ContratacaoEntity;
import br.com.dbccompany.vemsercoworking.Repository.ContratacaoRepository;
import org.springframework.stereotype.Service;

@Service
public class ContratacaoService extends ServiceAbstract<ContratacaoRepository, ContratacaoEntity, Integer> {
}
