package br.com.dbccompany.vemsercoworking.Service;

import br.com.dbccompany.vemsercoworking.Entity.ClientePacoteEntity;
import br.com.dbccompany.vemsercoworking.Repository.ClientePacoteRepository;
import org.springframework.stereotype.Service;

@Service
public class ClientePacoteService extends ServiceAbstract<ClientePacoteRepository, ClientePacoteEntity, Integer> {
}
