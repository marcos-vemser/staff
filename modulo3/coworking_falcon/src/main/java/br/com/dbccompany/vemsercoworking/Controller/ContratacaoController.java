package br.com.dbccompany.vemsercoworking.Controller;


import br.com.dbccompany.vemsercoworking.DTO.ContratacaoDTO;
import br.com.dbccompany.vemsercoworking.Entity.ContratacaoEntity;
import br.com.dbccompany.vemsercoworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {

    @Autowired
    ContratacaoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ContratacaoDTO> todosContratacao() {
        List<ContratacaoDTO> listaDTO = new ArrayList<>();
        for (ContratacaoEntity contratacao : service.todos()) {
            listaDTO.add(new ContratacaoDTO(contratacao));
        }
        return listaDTO;
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ContratacaoDTO salvar(@RequestBody ContratacaoDTO contratacaoDTO){
        ContratacaoEntity contratacaoEntity = contratacaoDTO.convert();
        ContratacaoDTO newDto = new ContratacaoDTO(service.salvar(contratacaoEntity));
        return newDto;
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public ContratacaoDTO contratacaoEspecifico(@PathVariable Integer id) {
        ContratacaoDTO contratacaoDTO = new ContratacaoDTO(service.porId(id));
        return contratacaoDTO;
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ContratacaoDTO editarContratacao(@PathVariable Integer id, @RequestBody ContratacaoDTO contratacaoDTO) {
        ContratacaoEntity contratacao = contratacaoDTO.convert();
        ContratacaoDTO newDTO = new ContratacaoDTO(service.editar(contratacao, id));
        return newDTO;
    }
}
