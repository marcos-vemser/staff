package br.com.dbccompany.vemsercoworking.Entity;

public enum TipoContratacaoEnum {
    MINUTO, HORA, TURNO, DIARIA, SEMANA, MES
}
