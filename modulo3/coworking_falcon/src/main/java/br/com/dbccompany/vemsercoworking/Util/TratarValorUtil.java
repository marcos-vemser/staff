package br.com.dbccompany.vemsercoworking.Util;

import java.text.NumberFormat;
import java.util.Locale;

public class TratarValorUtil {

    public static Double trataValorEntrada(String valor) {
        String auxiliar = valor.replaceAll("\\.", "");
        auxiliar = auxiliar.replaceAll("R\\$", "");
        auxiliar = auxiliar.replaceAll(" ", "");
        auxiliar = auxiliar.replaceAll(",", ".");
        double valorDouble = Double.parseDouble(auxiliar);
        return valorDouble;
    }

    public static String trataValorSaida(Double valor) {
        Locale BR = new Locale("pt","BR");
        NumberFormat nf = NumberFormat.getCurrencyInstance(BR);
        return nf.format(valor);
    }

}
