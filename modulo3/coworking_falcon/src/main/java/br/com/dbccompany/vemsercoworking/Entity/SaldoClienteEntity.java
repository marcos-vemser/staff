package br.com.dbccompany.vemsercoworking.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "SALDO_CLIENTE")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = SaldoClienteEntity.class)
public class SaldoClienteEntity extends EntityAbstract<SaldoClienteId> {

    @EmbeddedId
    private SaldoClienteId id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TipoContratacaoEnum tipoContratacao;

    @Column(nullable = false)
    private int quantidade;

    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate vencimento;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE", nullable = false, insertable = false, updatable = false)
    private ClienteEntity cliente;

    @ManyToOne
    @JoinColumn(name = "ID_ESPACO", nullable = false, insertable = false, updatable = false)
    private EspacoEntity espaco;

    @OneToMany( mappedBy = "saldoCliente")
    private List<AcessoEntity> acesso;

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public List<AcessoEntity> getAcesso() {
        return acesso;
    }

    public void setAcesso(List<AcessoEntity> acesso) {
        this.acesso = acesso;
    }
}
