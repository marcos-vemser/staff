package br.com.dbccompany.vemsercoworking.DTO;

import br.com.dbccompany.vemsercoworking.Entity.*;

import java.time.LocalDate;
import java.util.List;

public class SaldoClienteDTO {

    private SaldoClienteId id;
    private TipoContratacaoEnum tipoContratacao;
    private int quantidade;
    private LocalDate vencimento;
    private ClienteEntity cliente;
    private EspacoEntity espaco;
    private List<AcessoEntity> acesso;

    public SaldoClienteDTO() {}

    public SaldoClienteDTO(SaldoClienteEntity saldoCliente) {
        this.id = saldoCliente.getId();
        this.tipoContratacao = saldoCliente.getTipoContratacao();
        this.quantidade = saldoCliente.getQuantidade();
        this.vencimento = saldoCliente.getVencimento();
        this.cliente = saldoCliente.getCliente();
        this.espaco = saldoCliente.getEspaco();
        this.acesso = saldoCliente.getAcesso();
    }

    public SaldoClienteEntity convert() {
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(this.id);
        saldoCliente.setTipoContratacao(this.tipoContratacao);
        saldoCliente.setQuantidade(this.quantidade);
        saldoCliente.setVencimento(this.vencimento);
        saldoCliente.setCliente(this.cliente);
        saldoCliente.setEspaco(this.espaco);
        saldoCliente.setAcesso(this.acesso);
        return saldoCliente;
    }

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public List<AcessoEntity> getAcesso() {
        return acesso;
    }

    public void setAcesso(List<AcessoEntity> acesso) {
        this.acesso = acesso;
    }
}
