package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.vemsercoworking.Entity.SaldoClienteId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, SaldoClienteId> {
}
