package br.com.dbccompany.vemsercoworking.Repository;

import br.com.dbccompany.vemsercoworking.Entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.event.annotation.BeforeTestClass;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class ClientePacoteRepositoryTest {

    @Autowired
    PagamentoRepository repository;

    @Autowired
    ContratacaoRepository contratacaoRepository;

    @Autowired
    ClientePacoteRepository clientePacoteRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    EspacoRepository espacoRepository;

    @Autowired
    PacoteRepository pacoteRepository;

    @BeforeTestClass
    public static void init(){

    }

    @Test
    public void buscarPagamentoPorTipo(){
        //Pacote
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(12);

        //Cliente
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome("blabla");
        cliente.setCpf("12365478999");
        cliente.setDataNascimento(new Date(1992,10,10));

        ClienteEntity newCliente = clienteRepository.save(cliente);

        //Espaco
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("adasdas0");
        espaco.setQtdPessoas(10);
        espaco.setValor(100);

        //ClientePacote
        ClientePacoteEntity cliPacote = new ClientePacoteEntity();
        cliPacote.setCliente(newCliente);
        cliPacote.setPacote(pacoteRepository.save(pacote));
        cliPacote.setQuantidade(2);

        //Contratacao
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setCliente(newCliente);
        contratacao.setEspaco(espacoRepository.save(espaco));
        contratacao.setTipoContratacao(TipoContratacaoEnum.SEMANA);
        contratacao.setQuantidade(5);
        contratacao.setPrazo(30);

        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setClientePacote(clientePacoteRepository.save(cliPacote));
        pagamento.setContratacao(contratacaoRepository.save(contratacao));
        pagamento.setTipoPagamento(TipoPagamentoEnum.CREDITO);
        repository.save(pagamento);

        assertEquals(1, repository.findAllByTipoPagamento(TipoPagamentoEnum.CREDITO).size());
    }

}
