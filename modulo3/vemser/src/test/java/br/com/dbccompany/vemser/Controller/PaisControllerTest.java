package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.PaisEntity;
import br.com.dbccompany.vemser.Repository.PaisRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class PaisControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private static PaisRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoPais() throws Exception {
        URI uri = new URI("/api/pais/todos");

        mockMvc
            .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
            ).andExpect(MockMvcResultMatchers
                            .status()
                            .is(200)
            );
    }

    @Test
    public void salvarERetornarUmPais() throws Exception {
        URI uri = new URI("/api/pais/novo");
        String json = "{\"nome\": \"Brasil\"}";

        mockMvc
            .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
            ).andExpect(MockMvcResultMatchers
                    .jsonPath("$.id").exists()
            ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("Brasil")
            );
    }

    @Test
    public void deveRetornarUmPais() throws Exception {
        PaisEntity pais = new PaisEntity();
        pais.setNome("Argentina");

        PaisEntity newPais = repository.save(pais);

        mockMvc
            .perform(MockMvcRequestBuilders
                        .get("/api/pais/ver/{id}", newPais.getId())
                        .contentType(MediaType.APPLICATION_JSON)
            ).andExpect(MockMvcResultMatchers
                            .status()
                            .isOk()
            ).andExpect(MockMvcResultMatchers
                            .jsonPath("$.nome")
                            .value("Argentina")
            );
    }

    /*public void teste() throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        PaisEntity pais = new PaisEntity();
        pais.setNome("Brasil");

        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(pais);
        System.out.println(json);
    }*/
}
