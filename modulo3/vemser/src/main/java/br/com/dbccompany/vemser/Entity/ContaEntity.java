package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.util.List;

/* select * from pessoa

    EAGER -> Select * from pessoa inner join endereco

    pessoa: {
        nome: "",
        idade: 19,
        endereco: {
            logradouro: "",
            numero: 111
        }
    }

    LAZY -> select * from pessoa | endereco null

    pessoa: {
        nome: "",
        idade: 19,
        endereco: null
    }
*/

@Entity
public class ContaEntity extends EntityAbstract<ContaEntityId> {

    @EmbeddedId
    private ContaEntityId id;

    private int codigo;
    private double saldo;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_agencia" )
    private AgenciaEntity agencia;

    @ManyToOne
    @MapsId("id_tipo_conta")
    @JoinColumn( name = "ID_TIPO_CONTA", nullable = false )
    private TipoContaEntity tipoConta;

    @OneToMany( mappedBy = "conta")
    private List<MovimentacaoEntity> movimentacoes;

    @ManyToMany( cascade = CascadeType.ALL )
    @JoinTable( name = "CONTA_X_GERENTE",
            joinColumns = { @JoinColumn( name = "id_conta" ), @JoinColumn( name = "id_tipo_conta" ) },
            inverseJoinColumns = { @JoinColumn( name = "id_gerente") }
    )
    private List<GerenteEntity> gerentes;

    @OneToMany( mappedBy = "conta")
    private List<ContaClienteEntity> contasClientes;

    public ContaEntityId getId() {
        return id;
    }

    public void setId(ContaEntityId id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public AgenciaEntity getAgencia() {
        return agencia;
    }

    public void setAgencia(AgenciaEntity agencia) {
        this.agencia = agencia;
    }

    public TipoContaEntity getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(TipoContaEntity tipoConta) {
        this.tipoConta = tipoConta;
    }

    public List<MovimentacaoEntity> getMovimentacoes() {
        return movimentacoes;
    }

    public void setMovimentacoes(List<MovimentacaoEntity> movimentacoes) {
        this.movimentacoes = movimentacoes;
    }

    public List<GerenteEntity> getGerentes() {
        return gerentes;
    }

    public void setGerentes(List<GerenteEntity> gerentes) {
        this.gerentes = gerentes;
    }

    public List<ContaClienteEntity> getContasClientes() {
        return contasClientes;
    }

    public void setContasClientes(List<ContaClienteEntity> contasClientes) {
        this.contasClientes = contasClientes;
    }
}
