package br.com.dbccompany.vemser.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.List;

//Estado - ID_PAIS (RS, 1)(SC, 1)(PR, 1)(SP, 1)
//Pais (1, Brasil)

//Agencia ID_CONSOLIDACAO (2, AGENCIAWEB, 1)
//Consolidacao ID_AGENCIA (1, 2000, R$1500, 2)

//Conta
//Tabela Intermedia ( ID_CLIENTE, ID_CONTA ) (1, 1)(1, 2)(1, 3)(2, 1)(3, 1)(4, 1)
//Cliente

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EstadoEntity.class)
public class EstadoEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "ESTADO_SEQ", sequenceName = "ESTADO_SEQ")
    @GeneratedValue( generator = "ESTADO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String nome;

    @ManyToOne
    @JoinColumn( name = "ID_PAIS" )
    private PaisEntity pais;

    @OneToMany( mappedBy = "estado")
    private List<CidadeEntity> cidades;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public PaisEntity getPais() {
        return pais;
    }

    public void setPais(PaisEntity pais) {
        this.pais = pais;
    }

    public List<CidadeEntity> getCidades() {
        return cidades;
    }

    public void setCidades(List<CidadeEntity> cidades) {
        this.cidades = cidades;
    }
}
