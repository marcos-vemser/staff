package br.com.dbccompany.vemser.DTO;

import br.com.dbccompany.vemser.Entity.EstadoEntity;
import br.com.dbccompany.vemser.Entity.PaisEntity;

import java.util.List;

public class PaisDTO {

    private Integer id;
    private String nome;
    private List<EstadoEntity> estados;

    public PaisDTO( PaisEntity pais ) {
        this.id = pais.getId();
        this.nome = pais.getNome();
        this.estados = pais.getEstados();
    }

    public PaisEntity convert() {
        PaisEntity pais = new PaisEntity();
        pais.setId(this.id);
        pais.setNome(this.nome);
        pais.setEstados(this.estados);

        return pais;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<EstadoEntity> getEstados() {
        return estados;
    }

    public void setEstados(List<EstadoEntity> estados) {
        this.estados = estados;
    }
}
