package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.EntityAbstract;
import br.com.dbccompany.vemser.VemserApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public abstract class ServiceAbstract<
        R extends CrudRepository<E, T>,
        E extends EntityAbstract, T> {

    private Logger logger = LoggerFactory.getLogger(VemserApplication.class);

    @Autowired
    protected R repository;

    @Transactional( rollbackFor = Exception.class )
    public E salvar( E entidade ) {
        logger.warn("Se tiver campos faltando, não irá salvar a Entidade");
        try {
            return repository.save(entidade);
        }catch (Exception e) {
            //System.err.println(e.getMessage());
            //throw new RuntimeException(e.getCause());
            logger.error("Erro ao salvar entidade: " + e.getMessage());
            return null;
        }
    }

    @Transactional( rollbackFor = Exception.class )
    public E editar( E entidade, T id ) {
        entidade.setId(id);
        return repository.save(entidade);
    }

    public List<E> todos() {
        return (List<E>) repository.findAll();
    }

    public E porId( T id ) {
        return repository.findById(id).get();
    }
}
