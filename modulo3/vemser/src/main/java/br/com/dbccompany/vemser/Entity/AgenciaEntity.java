package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.util.List;


/*
 * Agencia: {
 *   codigo: 0001,
 *   nome: "",
 *   contas: [
 *   {},
 *   {}
 *   ]
 * }
 *
 * */

@Entity
public class AgenciaEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "AGENCIA_SEQ", sequenceName = "AGENCIA_SEQ")
    @GeneratedValue( generator = "AGENCIA_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private int codigo;
    private String nome;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_banco" )
    private BancoEntity banco;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "ID_ENDERECO")
    private EnderecoEntity endereco;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "ID_CONSOLIDACAO")
    private ConsolidacaoEntity consolidacao;

    @OneToMany( mappedBy = "agencia" )
    private List<ContaEntity> contas;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BancoEntity getBanco() {
        return banco;
    }

    public void setBanco(BancoEntity banco) {
        this.banco = banco;
    }

    public EnderecoEntity getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoEntity endereco) {
        this.endereco = endereco;
    }

    public ConsolidacaoEntity getConsolidacao() {
        return consolidacao;
    }

    public void setConsolidacao(ConsolidacaoEntity consolidacao) {
        this.consolidacao = consolidacao;
    }

    public List<ContaEntity> getContas() {
        return contas;
    }

    public void setContas(List<ContaEntity> contas) {
        this.contas = contas;
    }
}