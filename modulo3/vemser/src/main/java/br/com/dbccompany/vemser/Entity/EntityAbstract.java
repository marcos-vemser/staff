package br.com.dbccompany.vemser.Entity;

public abstract class EntityAbstract<T> {

    public abstract T getId();

    public abstract void setId(T id);

}
