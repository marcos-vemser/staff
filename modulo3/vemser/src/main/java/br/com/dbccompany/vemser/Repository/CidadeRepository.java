package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.CidadeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CidadeRepository extends CrudRepository<CidadeEntity, Integer> {
}
