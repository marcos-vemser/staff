package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.EstadoEntity;
import br.com.dbccompany.vemser.Repository.EstadoRepository;
import org.springframework.stereotype.Service;

@Service
public class EstadoService extends ServiceAbstract<EstadoRepository, EstadoEntity, Integer> {
}
