package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.PaisEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaisRepository extends CrudRepository<PaisEntity, Integer> {

    PaisEntity findById( int id );
    PaisEntity findByNome( String nome );
    List<PaisEntity> findAll();
    List<PaisEntity> findAllByNome( String nome );
    //PaisEntity findByIdAndNome( int id, String nome );

}
