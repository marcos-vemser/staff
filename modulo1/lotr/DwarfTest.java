import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest {
    
    @Test
    public void dwarfNasceCom110DeVida() {
        Dwarf dwarf = new Dwarf("Brostaeni");
        assertEquals(110.0, dwarf.getVida(), .000001);
        assertEquals( Status.RECEM_CRIADO, dwarf.getStatus() );
    }
    
    @Test
    public void dwarfPerdeDezDeVida() {
        Dwarf dwarf = new Dwarf("Brostaeni");
        dwarf.sofrerDano();
        assertEquals(100.0, dwarf.getVida(), .01);
    }
    
    @Test
    public void dwarfPerdeDezDeVidaDuasVezes() {
        Dwarf dwarf = new Dwarf("Brostaeni");
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        assertEquals(90.0, dwarf.getVida(), .01);
        assertEquals(Status.SOFREU_DANO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfZeraVida() {
        Dwarf dwarf = new Dwarf("Brostaeni");
        
        for(int i = 0; i < 12;i++) {
            dwarf.sofrerDano();
        }
        
        assertEquals(0.0, dwarf.getVida(), .01);
        assertEquals(Status.MORTO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfNasceComEscudoNoInventario() {
        Dwarf dwarf = new Dwarf("Gimli");
        Item esperado = new Item(1, "Escudo");
        Item resultado = dwarf.getInventario().obter(0);
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void dwarfEquipaEscudoETomaMetadeDano() {
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.equiparEscudo();
        dwarf.sofrerDano();
        assertEquals(105.0, dwarf.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfNaoEquipaEscudoETomaDanoIntegral() {
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.sofrerDano();
        assertEquals(100.0, dwarf.getVida(), 1e-9);
    }
    
    
    
    
    
}