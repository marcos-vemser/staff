import java.util.*;

public class EstrategiaPriorizandoElfosVerdes implements EstrategiaDeAtaque {

    private ArrayList<Elfo> bubble(ArrayList<Elfo> elfos) {
        boolean houveTroca = true;
        while( houveTroca ) {
            houveTroca = false;
            
            for (int i = 0; i <  elfos.size(); i++) {
                Elfo elfoAtual = elfos.get(i);
                Elfo elfoProximo = elfos.get(i + 1);
                
                boolean precisaTrocar = elfoAtual instanceof ElfoNoturno &&
                                        elfoProximo instanceof ElfoVerde;
                                        
                if (precisaTrocar) {
                    elfos.set(i, elfoProximo);
                    elfos.set(i + 1, elfoAtual);
                    houveTroca = true;
                }
            }
        }
        return elfos;
    }
    
    // https://docs.oracle.com/javase/8/docs/api/java/util/Comparator.html#compare-T-T-
    private ArrayList<Elfo> colletions(ArrayList<Elfo> elfos) {
        //Collections.sort(elfos, new ComparadorDeElfos());
        Collections.sort(elfos, new Comparator<Elfo>(){
            public int compare( Elfo elfoAtual, Elfo elfoProximo ) {
                boolean mesmoTipo = elfoAtual.getClass() == elfoProximo.getClass();
                
                if (mesmoTipo) {
                    return 0;
                }
                
                return elfoAtual instanceof ElfoVerde &&
                elfoProximo instanceof ElfoNoturno ? -1 : 1;
            }
        });
        
        return elfos;
        
    }
    
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes) {
        return colletions(atacantes);
    };
}