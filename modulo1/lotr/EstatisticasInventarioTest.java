import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest {
    @Test
    public void calcularMediaInventarioVazio() {
        Inventario inventario = new Inventario(1);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertTrue(Double.isNaN(resultado));
    }
    
    @Test
    public void calcularMediaUmItem() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(3, "Escudo de Madeira"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals(3, resultado, 1e-8);
    }
    
    @Test
    public void calcularMediaQtdsIguais() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(3, "Escudo de Madeira"));
        inventario.adicionar(new Item(3, "Espada de Cristal"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals(3, resultado, 1e-8);
    }
    
    @Test
    public void calcularMediaQtdsDiferentes() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(4, "Escudo de Madeira"));
        inventario.adicionar(new Item(4, "Espada de Cristal"));
        inventario.adicionar(new Item(1, "Botas"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals(3, resultado, 1e-8);
    }
    
    @Test
    public void calcularMedianaApenasUmItem() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(6, "Flechas de Gelo"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals(6, resultado, 1e-8);
    }
    
    @Test
    public void calcularMedianaQtdImpar() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(5, "Escudo de Madeira"));
        inventario.adicionar(new Item(10, "Espada de Cristal"));
        inventario.adicionar(new Item(20, "Botas"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals(10, resultado, 1e-8);
    }
    
    @Test
    public void calcularMedianaQtdPar() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(5, "Escudo de Madeira"));
        inventario.adicionar(new Item(10, "Espada de Cristal"));
        inventario.adicionar(new Item(20, "Botas"));
        inventario.adicionar(new Item(30, "Adaga"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals(15, resultado, 1e-8);
    }
    
    @Test
    public void qtdItensAcimaDaMediaComApenasUmItem() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(5, "Escudo de Madeira"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.qtdItensAcimaDaMedia();
        assertEquals(0, resultado, 1e-8);
    }
    
    @Test
    public void qtdItensAcimaDaMediaComVariosItens() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(5, "Escudo de Madeira"));
        inventario.adicionar(new Item(10, "Espada de Cristal"));
        inventario.adicionar(new Item(20, "Botas"));
        inventario.adicionar(new Item(30, "Adaga"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.qtdItensAcimaDaMedia();
        //media = 16,25
        assertEquals(2, resultado, 1e-8);
    }
    
    @Test
    public void qtdItensAcimaDaMediaComItensIgualMedia() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(1, "Escudo de Madeira"));
        inventario.adicionar(new Item(2, "Espada de Cristal"));
        inventario.adicionar(new Item(3, "Botas"));
        inventario.adicionar(new Item(4, "Adaga"));
        inventario.adicionar(new Item(5, "Poção"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.qtdItensAcimaDaMedia();
        //media = 3
        assertEquals(2, resultado, 1e-8);
    }
    
    
    

}