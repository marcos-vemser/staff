public abstract class Personagem {
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida;
    protected int experiencia, qtdExperienciaPorAtaque;
    protected double qtdDano;
    
    {
        this.status = Status.RECEM_CRIADO;
        this.inventario = new Inventario(0);
        this.experiencia = 0;
        this.qtdExperienciaPorAtaque = 1;
        this.qtdDano = 0.0;
    }
    
    public Personagem( String nome ) {
        this.nome = nome;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNome( String nome ) {
        this.nome = nome;
    }
    
    public Status getStatus(){
        return this.status;
    }
    
    public Inventario getInventario() {
        return this.inventario;
    }
    
    public double getVida() {
        return this.vida;
    }
    
    public int getExperiencia() {
        return this.experiencia;
    }
    
    public void ganharItem( Item item ) {
        this.inventario.adicionar( item );
    }
    
    public void perderItem( Item item ) {
        this.inventario.remover(item);
    }
    
    protected void aumentarXP() {
        this.experiencia += this.qtdExperienciaPorAtaque;
    }
    
    protected double calcularDano() {
        return this.qtdDano;
    }
    
    private boolean estaVivo() {
        return this.vida > 0;
    }
    
    public void sofrerDano() {
        this.vida -= this.vida >= calcularDano() ? calcularDano() : this.vida;
        this.status = estaVivo() ? Status.SOFREU_DANO : Status.MORTO;
    }
    
    public abstract String imprimirNomeDaClasse();
    
    
}