import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class EstrategiaPriorizandoElfosVerdesTest  {
    @Test
    public void exercitoEmbaralhadoPriorizandoAtaqueComElfosVerde() {
        EstrategiaPriorizandoElfosVerdes estrategia = new EstrategiaPriorizandoElfosVerdes();
        Elfo night1 = new ElfoNoturno("Night 1");
        Elfo night2 = new ElfoNoturno("Night 2");
        Elfo green1 = new ElfoVerde("Green 1");
        Elfo night3 = new ElfoNoturno("Night 3");
        Elfo green2 = new ElfoVerde("Green 2");
        
        ArrayList<Elfo> elfosEntrada = new ArrayList<>(
            Arrays.asList(night1, night2, green1, night3, green2)
        );
        
        ArrayList<Elfo> esperado = new ArrayList<>(
            Arrays.asList(green1, green2, night1, night2, night3)
        );
        
        ArrayList<Elfo> obtido = estrategia.getOrdemDeAtaque(elfosEntrada);
        
        assertEquals(esperado, obtido);
    }
    
    @Test
    public void exercitoSoElfosVerdesPriorizandoAtaque() {
        EstrategiaPriorizandoElfosVerdes estrategia = new EstrategiaPriorizandoElfosVerdes();
        Elfo green1 = new ElfoVerde("Green 1");
        Elfo green2 = new ElfoVerde("Green 2");
        
        ArrayList<Elfo> elfosEntrada = new ArrayList<>(
            Arrays.asList(green1, green2)
        );
        
        ArrayList<Elfo> esperado = new ArrayList<>(
            Arrays.asList(green1, green2)
        );
        
        ArrayList<Elfo> obtido = estrategia.getOrdemDeAtaque(elfosEntrada);
        
        assertEquals(esperado, obtido);
    }
}