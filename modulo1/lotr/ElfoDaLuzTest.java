import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ElfoDaLuzTest{
    
    private static final double DELTA = 1e-1;

    @Test
    public void elfoDaLuzNasceComEspada() {
        Elfo feanor = new ElfoDaLuz("Feanor");
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item(1, "Arco"),
                    new Item(2, "Flecha"),
                    new Item(1, "Espada de Galvorn")
                ));
        assertEquals(esperado, feanor.getInventario().getItens());
    }

    @Test
    public void elfoDaLuzDevePerderVida() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        Dwarf gul = new Dwarf("Gul");
        feanor.atacarComEspada(gul);
        assertEquals(79, feanor.getVida(), DELTA);
        assertEquals(100, gul.getVida(), DELTA);
    }

    @Test
    public void elfoDaLuzDeveGanharVida() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        Dwarf gul = new Dwarf("Gul");
        feanor.atacarComEspada(gul);
        feanor.atacarComEspada(gul);
        assertEquals(89, feanor.getVida(), DELTA);
        assertEquals(90, gul.getVida(), DELTA);
    }

    @Test
    public void elfoDaLuzMorreAoAtacarTantasVezes() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        Dwarf gul = new Dwarf("Gul");
        for (int i = 0; i < 17; i++) {
            feanor.atacarComEspada(gul);
        }
        assertEquals(0, feanor.getVida(), DELTA);
        assertEquals(0, gul.getVida(), DELTA);
        assertEquals(Status.MORTO, feanor.getStatus());
        assertEquals(Status.MORTO, gul.getStatus());
    }

    @Test
    public void elfoDaLuzNaoPodePerderEspadaDeGalvorn() {
        Elfo feanor = new ElfoDaLuz("Feanor");
        feanor.perderItem(new Item(1, "Espada de Galvorn"));
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item(1, "Arco"),
                    new Item(2, "Flecha"),
                    new Item(1, "Espada de Galvorn")
                ));
        assertEquals(esperado, feanor.getInventario().getItens());
    }

    @Test
    public void elfoDaLuzPodePerderArco() {
        Elfo feanor = new ElfoDaLuz("Feanor");
        feanor.perderItem(new Item(1, "Arco"));
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
                    new Item(2, "Flecha"),
                    new Item(1, "Espada de Galvorn")
                ));
        assertEquals(esperado, feanor.getInventario().getItens());
    }

    @Test
    public void elfoDaLuzSoAtacaComUnidadeDeEspada() {
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        feanor.getInventario().getItens().get(2).setQuantidade(0);
        feanor.atacarComEspada(new Dwarf("Gul"));
        assertEquals(79, feanor.getVida(), DELTA);
    }
}
