import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class AgendaContatosTest {

    @Test
    public void adicionarContatoEPesquisa(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "99999-9999");
        String resultado = agenda.consultar("Marcos");
        assertEquals("99999-9999", resultado);
    }
    
    @Test
    public void adicionarContatoEPesquisaPeloTelefone(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "99999-9999");
        String resultado = agenda.consutarPeloTelefone("99999-9999");
        assertEquals("Marcos", resultado);
    }
    
    @Test
    public void adicionarDoisContatosEGerarCSV(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos", "99999-9999");
        agenda.adicionar("Marcos 2", "99999-9889");
        String separador = System.lineSeparator();
        String resultado = String.format("Marcos,99999-9999%sMarcos 2,99999-9889%s", separador,separador);
        assertEquals(resultado, agenda.csv());
    }
    
}